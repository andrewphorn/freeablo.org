---
layout: page
title: About
permalink: /about/
---

Hello my friend, stay a while and listen!

freeablo is a modern cross platform reimplementation of the game engine used in Diablo 1. As it is just an engine, you will need the original data files to play the game.

Currently, you can run around town, players and npcs animate, and you can go down into the first few levels of the dungeon (which is randomly generated). This is just a base for a game so far, so if you can help please see the github!
