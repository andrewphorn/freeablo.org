---
layout: page
title: Media
permalink: /media/
---

v0.3 release video:
<iframe src="//www.youtube.com/embed/AwGktV5_N4g" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

v0.2 release video:
<iframe src="//www.youtube.com/embed/qA3SrOkJzgw" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Messing around in v0.1:
<iframe src="//www.youtube.com/embed/QFrU1szmPZk" width="420" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
