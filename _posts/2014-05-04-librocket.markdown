---
layout: post
title: "libRocket"
date: 2014-05-04
---

I have been planning to use [libRocket](http://librocket.com/) as the gui library for freeablo. The great thing about it is that it allows you to design guis with a derivative of html and css, so this would be very user-moddable.

However, freeablo is using SDL for rendering, and libRocket has no native support for SDL. Fortunately, it does have a nice interface for plugging in rendering backends, and someone has written an sdl backend for it ([http://mdqinc.com/blog/2013/01/integrating-librocket-with-sdl-2/](http://mdqinc.com/blog/2013/01/integrating-librocket-with-sdl-2/)). Unfortunately, this code was rather tied up in the game engine it was created for, and was not a drop-in solution.

It was a massive pain getting it to work, but I finally managed to do so. I have submitted a [pull request](https://github.com/lloydw/libRocket/pull/122) to the libRocket authors, in the hopes that this will prevent this pain from happening ever again, but I'm not sure if there's anyone actually reviewing pull requests on their end. Anyway, the code can be found here: [https://github.com/wheybags/libRocket/tree/sdl2/Samples/basic/sdl2](https://github.com/wheybags/libRocket/tree/sdl2/Samples/basic/sdl2)

Unfortunately, this does mean SDL 1 support will have to be dropped, but to be fair, the reasons for it existing were [fairly silly](http://www.reddit.com/r/gamedev/comments/23oimx/freeablo_a_cross_platform_foss_rewrite_of_the/cgz4ojo) :P
