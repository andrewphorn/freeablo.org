---
layout: post
title: "Media section"
date: 2014-11-14
---

Another short update - I made a media section with some screenshots + a basic video: [here]({{ "media" | relative_url }})

BTW: If any of you out there are good at web (or just ordinary) design, it would be nice to get custom wordpress + phpbb themes (that match), and/or a freeablo logo.
